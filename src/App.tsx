import React,{useState,useEffect} from 'react';
import logo from './logo.svg';
import './App.css';
import StockPage from './components/StockPage';
import UserLogin from './components/UserLogin';
import { AwsUtil } from './components/aws/aws-util';

function App() {

  const [userLoggedIn, setUserLoggedIn] = useState<boolean>(false);

  useEffect(() => {
    
    sessionDetails();

  },[]);


 

  const sessionDetails=(): void =>{
    AwsUtil.getSessionInfo().then(data => {
      setUserLoggedIn(true);
    }).catch(err => {
      setUserLoggedIn(false);
    });
  }

  return (
    <UserLogin/>
   
  );
}

export default App;

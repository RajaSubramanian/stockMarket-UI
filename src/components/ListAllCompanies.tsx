import React from 'react';

interface  CompanyDetails{

    companyDetails:any[]

}


const ListAllCompanies = (props:CompanyDetails)=>{

    return(
        <div className="row justify-content-start">

<div className="col-2">

               </div>

               <div className="col-8">
               <table className="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Company Name</th>
      <th scope="col">Company Code</th>
    </tr>
  </thead>
  <tbody>
    {
        props.companyDetails.map(( t, index ) =>{
            return(<tr key={index}>
                <td>{index+1}</td>
                <td>{t.companyName}</td>
                <td>{t.companyCode}</td>
            </tr>)
        })
    }
  </tbody>
</table>


                   </div>

    </div>
    );
}

export default ListAllCompanies;
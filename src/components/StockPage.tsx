import React, {useEffect, useState } from 'react';
import Header from './Header';
import stocklogo from '../stocklogo.jpg';
import AddCompany from './AddCompany';
import StockDisplay from './StocksDisplay';
import ListAllCompanies from './ListAllCompanies';

const mockStockData=[
    
        {
            "id": 8,
            "stockPrice": 1234.6,
            "createDt": "2021-07-03T12:52:02.931+00:00",
            "companyCd": "TCS"
        }
        
    
]


const StockPage =()=>{
    
const [gridDisplay, setGridDispaly] = useState<boolean>(false);

const [formDisplay, setFormDispaly] = useState<boolean>(false);

const [companyDisplay, setCompanyDispaly] = useState<boolean>(true);

const [dateFrom, setFromDate] = useState<any>(null);

const [dateTo, setToDate] = useState<any>(null);

const [companyCd, setCompanyCd] = useState<any>(null);

const [stockDetails, setStockDetails] = useState<any[]>([]);

const [companyDetails, setCompanyDetails] = useState<any[]>([]);

console.log(stockDetails);

useEffect(() => {
    
    fetch("http://localhost:8082/api/v1.0/market/company/getall")
    .then(response => response.json())
        
    .then(res => setCompanyDetails(res))

  },[]);

  

  


const findStockDetails=()=>{

    fetch("http://localhost:8082/api/v1.0/market/stock/get/"+companyCd+"/"+dateFrom+"/"+dateTo)
    .then(response => response.json())
        // 4. Setting *dogImage* to the image url that we received from the response above
    .then(res => setStockDetails(res))
setCompanyDispaly(false);
    setGridDispaly(true);
    setFormDispaly(false);
}
const addCompany=()=>{
setCompanyDispaly(false);

    setGridDispaly(false);
    setFormDispaly(true);
}

const getCompanyDisplay=()=>{
    setCompanyDispaly(true);
    setGridDispaly(false);
    setFormDispaly(false);
}

   return(
       <div><Header/>
       <div className="container-fluid">
       <div className="row justify-content-start">
       <div className="col-2">
       <img className ="left" src={stocklogo} alt="logo" />
       </div>
       <div className="col-2">
       <button type="button" className="btn btn-primary" onClick={addCompany}>AddCompany</button>
       </div>
       <div className="col-2">
       <button type="button" className="btn btn-primary" onClick={()=>getCompanyDisplay()}>List All Companies</button>
       </div>
       <div className="col-2">
       <div className="form-outline">
       <select className="form-select" aria-label="Default select example" onChange={(e)=>setCompanyCd(e.target.value)}>
  <option selected>select</option>
  {companyDetails.map((op,index)=>{
            
            return <option key={index} value={op.companyCode}>{op.companyName}</option>
        })}
</select>
  
  
  </div>
  
</div>
<div className="col-2">
  <button className="btn btn-secondary" type="button" onClick={findStockDetails}>
    search
      </button>
  </div>

       </div>
       
       <div className="row justify-content-middle">
         <div className="col-4"></div>
         <div className="col-4">
     <label htmlFor="from">From:</label>
    <input type="date" id="from" name="from" onChange={(e)=>setFromDate(e.target.value)}></input>
</div>
<div className="col-4">
    <label htmlFor="to">To:</label>
    <input type="date" id="to" name="to" onChange={(e)=>setToDate(e.target.value)}></input>
</div>

     </div>
     {!gridDisplay && formDisplay?<AddCompany/>:undefined}

     

     {companyDisplay?<ListAllCompanies companyDetails={companyDetails}/>:undefined}
     
     {gridDisplay?<StockDisplay stockpriceDetails={stockDetails}/>:undefined}

       </div>
       </div>
       
       );

}

export default StockPage;
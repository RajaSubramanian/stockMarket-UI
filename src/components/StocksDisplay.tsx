import React,{useState,useEffect} from 'react';

interface StockDisplay{

    stockpriceDetails:any[]

}


const StockDisplay =(props:StockDisplay)=>{


  const [minStock, setMinStock] = useState<any>(null);

  const [maxStock, setMaxStock] = useState<any>(null);

  const [avgStock, setAvgStock] = useState<any>(null);

  
  //console.log(minStock);

  const getMinStockCal=(input:any)=>{
    let arr: any[] = []
    input.map((s:any)=>{
      arr.push(s.stockPrice);
    })
    
    return Math.min(...arr);
  }

  const getMaxStockCal=(input:any)=>{
    let arr: any[] = []
    input.map((s:any)=>{
      arr.push(s.stockPrice);
    })
    
    return Math.max(...arr);
  }

  const getAvgStockCal=(input:any)=>{
    let arr: any[] = []
    let average;
    input.map((s:any)=>{
      arr.push(s.stockPrice);
    })
   
    return arr.length>0?arr.reduce((a, b) => a + b) / arr.length:arr;
  }

  const getFormattedDate=(input:any)=>{

    let date = new Date(input);

   return date.getDate()+"/"+(date.getMonth()+1)+"/"+date.getFullYear()

  }

  const getFormattedTime=(input:any)=>{

    let date = new Date(input);

   return date.getHours()+"h"+"-"+date.getMinutes()+"m"+"-"+date.getSeconds()+"Sec"

  }

return(
    <div className="row justify-content-start">

<div className="col-2">

               </div>

               <div className="col-8">
               <table className="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">StockPrice</th>
      <th scope="col">Date</th>
      <th scope="col">Time</th>
    </tr>
  </thead>
  <tbody>
    {
        props.stockpriceDetails.map(( t, index ) =>{
            return(<tr key={index}>
                <td>{index+1}</td>
                <td>{t.stockPrice}</td>
                <td>{getFormattedDate(t.createDt)}</td>
                <td>{getFormattedTime(t.createDt)}</td>
            </tr>)
        })
    }
  </tbody>
</table>
{props.stockpriceDetails.length>0?(
  <div className="card" >
  <div className="card-body">
    
    <h6 className="card-subtitle mb-2 text-muted">MaxStockPrice: {getMaxStockCal(props.stockpriceDetails)}</h6>
    <h6 className="card-subtitle mb-2 text-muted">MinStockPrice: {getMinStockCal(props.stockpriceDetails)}</h6>
    <h6 className="card-subtitle mb-2 text-muted">AvgStockPrice: {getAvgStockCal(props.stockpriceDetails)}</h6>
    
    
  </div>
</div>
):undefined}

                   </div>

    </div>
);

}

export default StockDisplay;
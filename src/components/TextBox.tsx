import ReactDOM from 'react-dom';
import { Form } from "react-bootstrap";

interface textbox{
    controlId:any,
    type:any,
    label:any,
    onChange?:(event:any)=>void;
    feedback:any,
    min?:any

}


const TextBox =(props:textbox)=>{

return (
    <div id='text-box-widget'>
                <Form.Group className='mb-3 rgstr-comp-field' controlId={props.controlId}>
                    <Form.Label>{props.label}</Form.Label>
                    <Form.Control required type={props.type} placeholder={props.label} name={props.controlId}
                        onChange={props.onChange} autoComplete='off' min={props.min}/>
                    <Form.Control.Feedback type="invalid">{props.feedback}</Form.Control.Feedback>
                </Form.Group>
            </div>
)


}
export default TextBox;

import ReactDOM from 'react-dom';
import React, { Fragment,useState } from "react";
import { Button, Card, Form } from "react-bootstrap";
import TextBox from './TextBox';
import { AwsUtil } from "./aws/aws-util";
import { CognitoUserAttribute, ICognitoUserAttributeData } from "amazon-cognito-identity-js";
import './UserLogin.scss';
import { findAllByTestId } from '@testing-library/react';

const UserLogin =()=>{

    let LOGIN_TITLE = 'Login';
    let SIGN_UP_TITLE = 'Sign Up';
    let LOGIN_ACT_LINK_TEXT = 'Sign Up Now !';
    let SIGN_UP_LINK_TEXT = 'Go Back to Login';

    

    const [templateval, setTemplateval] = useState<template>({title: LOGIN_TITLE,
        actionLinkText: LOGIN_ACT_LINK_TEXT});

        const [userval, setUserval] = useState<any>({ name: '', email: '',
        password: ''});

        const [isFormInValid, setIsFormInvalid] = useState<boolean>(false);

        const [errorMsg, setErrorMsg] = useState<error>({errorMsg:''});

        

        const [formError, setFormError] = useState<boolean>(false);

    const toggleTemplate=()=>{
        if(templateval.title==LOGIN_TITLE){

            setTemplateval({title: SIGN_UP_TITLE,
                actionLinkText: SIGN_UP_LINK_TEXT});
            }
        else{

            setTemplateval({title: LOGIN_TITLE,
                actionLinkText: LOGIN_ACT_LINK_TEXT});
            }

        }
        
type error = {
    errorMsg:string;
}

 type template = {
    title:string;
    actionLinkText:string;    

}

type userDetails = {
        name: any,
        email: any,
        password: any
}
    const templatePage: template = {
        title: LOGIN_TITLE,
        actionLinkText: LOGIN_ACT_LINK_TEXT
    };

    /**
     * This method to call aws cognito for signup
     * 
     */
    const signUp=(): void =>{
       // this.context.showLoadingSpinner();
        const nameAttribute: ICognitoUserAttributeData = { Name: 'name', Value: userval.name }
        const name: CognitoUserAttribute = new CognitoUserAttribute(nameAttribute);
        AwsUtil.signUpUser(userval?.email, userval?.password, [name]).then((data) => {
            alert(userval?.email);
            const otp: any = prompt("Please enter your OTP", "");
            conformSignUp(userval?.email, otp);
          //  this.context.triggerToast('Sucessfully Registered..!');
            setTimeout(() => {toggleTemplate()}, 2000);
        }).catch(err => {
            console.log("sign Error");
            setErrorMsg(err.message);
        }).finally(() => {
           // this.context.hideLoadingSpinner();
        });
    }

    
    /**
     * This method to listen input change event to capture form field data
     * 
     * @param event 
     */
    const onChange=(event: any)=> {
        const name = event.target.name;
        const value = event.target.value;
        console.log(name);
        console.log(value);
        userval[name]=value;
        console.log(userval);
        // this.setState((previousState: any) => {
        //     const userInfo = Object.assign({}, previousState.userInfo);
        //     userInfo[name] = value;
        //     return { userInfo };
        // });
        //setUserval({name:value,})
    }

    
    /**
     * To comform user registration
     * 
     * @param email 
     * @param code 
     * @returns 
     */
    const conformSignUp=(email: string, code: string): Promise<any> => {
        return new Promise((resolve) => {
            AwsUtil.conformSignUp(email).confirmRegistration(code, true, (err: any, result: any) => {
                if (err) {
                    return resolve({ statusCode: 422, response: err });
                }
                return resolve({ statusCode: 400, response: result });
            });
        });
    }

    /**
     * This method to call aws cognito for login
     */
    const login=(): void => {
      //  this.context.showLoadingSpinner();
        AwsUtil.authenticateUser(userval.email, userval.password)
            .then((data: any) => {
                alert("Atuthenticated");
               // this.context.updateLoginStatus(true);
            }).catch((err: any) => {
                alert("Error");
                setErrorMsg(err.message);
            }).finally(() => {
               // this.context.hideLoadingSpinner();
            });
    }

    /**
     * To handel form submit
     * 
     * @param event 
     */
    const handleSubmit=(event: any): void => {
        alert("444"+templateval.title);
        alert("33"+event);
        console.log(event);
        if(validation()) {
            alert("valid");
            setFormError(false);
            if(templateval?.title === LOGIN_TITLE) {
                login();
            } else {
               signUp();
            }
        } else {
           setFormError(true);
        }
       
    }


    /**
     * Validation
     * 
     * @returns 
     */
    const validation=(): boolean=> {
        console.log(userval?.email);
        return userval?.email?.length > 0
            && userval?.password?.length > 0
            && (templateval.title === LOGIN_TITLE || userval?.name?.length > 0);
    }

   
    
        return (
//             <div className="container h-100 d-flex justify-content-center">
//        <div className="jumbotron my-auto">
//                 <form>
//   <div className="form-group">
//     <label htmlFor="exampleInputEmail1">Email address</label>
//     <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email"/>
//     <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small>
//   </div>
//   <div className="form-group">
//     <label htmlFor="exampleInputPassword1">Password</label>
//     <input type="password" className="form-control" id="exampleInputPassword1" placeholder="Password"/>
//   </div>
//   <div className="form-group form-check">
//     <input type="checkbox" className="form-check-input" id="exampleCheck1"/>
//     <label className="form-check-label" htmlFor="exampleCheck1">Check me out</label>
//   </div>
//   <button type="submit" className="btn btn-primary">Submit</button>
// </form>
            
//        </div>
            //  </div>
           <div id='login-user'>
            <Card bg='primary' text='white' className="mb-2">
            <Card.Header>
                    <Card.Title className='card-title'>{templateval.title}</Card.Title>
                </Card.Header>

                <Card.Body>
                <Form noValidate validated={isFormInValid} onSubmit={handleSubmit}>
                <TextBox label='Email' type='email' onChange ={onChange} controlId='email'
                             feedback='Please provide valid email' />
                {templateval.title==SIGN_UP_TITLE?<TextBox label='Name' onChange ={onChange} type='text' controlId='name'
                                 feedback='Please enter your name' />:undefined}
                <TextBox label='Password' type='password' onChange ={onChange} controlId='password' 
                             feedback='Please enter valid password' />
                {templateval.title==SIGN_UP_TITLE?<div className='password-help-text'>Password should alpha numaric with 1 SPL char</div>:undefined} 
                 <Button  type='submit'  className='button btn-danger'>{templateval.title}</Button>            
                </Form>
                <div className='action-text'><span onClick={toggleTemplate}>{templateval.actionLinkText}</span></div>
                </Card.Body>
            </Card>

            </div>
         
        );
    };
    
 
 
 
 export default UserLogin;
import { CognitoUserPool} from 'amazon-cognito-identity-js';

const poolData = {
    UserPoolId: "ap-south-1_WwnxFsIva",
    ClientId: "4o4cc253k3ca57efjp4tbt1fs2"
}

export default new CognitoUserPool(poolData);
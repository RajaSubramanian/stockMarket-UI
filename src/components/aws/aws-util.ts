import { AuthenticationDetails, CognitoUser, CognitoUserAttribute,  } from "amazon-cognito-identity-js";
import Pool from './aws-cognito-pool';

export class AwsUtil {

    /**
     * This method to check authentication of user
     * 
     * @param Username 
     * @param Password 
     * @returns 
     */
    public static authenticateUser(Username: string, Password: string): Promise<any> {
        return new Promise((resolve, reject) => {
            const userInfo = new CognitoUser({ Username, Pool });
            const authDetails = new AuthenticationDetails({ Username, Password });
            userInfo.authenticateUser(authDetails, {
                onSuccess: (data) => {
                    resolve(data);
                },
                onFailure: (data) => {
                    reject(data)
                },
                newPasswordRequired: (data) => {
                    reject(data);
                }
            });
        });
    }

    /**
     * This method to get session info
     * 
     * @returns 
     */
    public static getSessionInfo(): Promise<any> {
        return new Promise((resolve, reject) => {
            const userInfo = Pool.getCurrentUser();
            if (userInfo) {
                userInfo.getSession((err: any, session: any) => {
                    if (err) {
                        resolve(err);
                    } else {
                        resolve(session);
                    }
                });
            } else {
                reject();
            }
        });
    }

    /**
     * This method to signup user with below details
     * email, password, name
     * 
     * @param username 
     * @param password 
     * @param userAttributes 
     * @returns 
     */
    public static signUpUser(username: string, password: string, userAttributes: CognitoUserAttribute[]): Promise<any> {
        alert("userrr"+username);
        alert("pas"+password);
        alert(userAttributes);
        return new Promise((resolve, reject) => {
            Pool.signUp(username, password, userAttributes, [], (err, data) => {
                if (err) {
                    alert("userrrErrorrr");
                    reject(err);
                } else {
                    alert("elseee");
                    resolve(data);
                }
            });
        });
    }


    /**
     * Method to confor the user signup
     * 
     * @param email 
     * @returns 
     */
    public static conformSignUp(email: string) {
        const userData = {
            Username: email,
            Pool: Pool
          };
          return new CognitoUser(userData);
    }

}